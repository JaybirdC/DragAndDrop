$(document).ready(
  function() {
    $('li.CategoryFile').mousedown(
      function() {
        $('li.CategoryFile').not(this).removeClass('CategoryFileSelected');
        $(this).addClass('CategoryFileSelected');
      }
    );
    $('ul#CategoryFiles').sortable();
  }
);