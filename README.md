## Drag and Drop Demo

This is an exercise in playing around with the Draggable module of the jQuery-UI library.

Draggable is one among many functions of the jQuery-UI library, allowing users to interact and rearrange items within the interface onClick. Here you can reorder the items in the list by simply clicking and dragging. The links refer to no actual destinations.

_Document Icon obtained from https://pixabay.com/en/document-html-web-internet-public-98478/._